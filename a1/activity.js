//$count operator
db.fruits.aggregate([
            {$match: {stock: {$gte: 20} } },
            {$count: "enoughStocks"}
        ]);

//$avg operator

db.fruits.aggregate([
           {$match: {onSale: true}},
           {$group: {_id: "$supplier_id", avg_price: {$avg: "$price"} } },
        ]);

//$max operator

db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", maxPrice: {$max: "$price"} } },
        ]);

//$min operator

db.fruits.aggregate([
            {$match: {onSale: true}},
            {$group: {_id: "$supplier_id", minPrice: {$min: "$price"} } },
        ]);